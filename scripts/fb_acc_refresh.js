var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

phantom.addCookie({'name': 'reg_fb_ref', 'value': 'https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfeFKhDm07usUcZ9YEb4rMH51pFwUntUvs2vFky4hGvK_vhLgwBwhHCgXRs5qPysA4u0bEuAyvSLgk9Pkx3zH1oKQuiZvEN5KMx47BFUoc8Alg%26smuh%3D46396%26lh%3DAc9YslrRvglVPfu2', 'domain': '.facebook.com', 'path': '/',
    'httponly': false, 'secure': false, 'expires': (new Date()).getTime() + 3600 * 24 * 30
});
phantom.addCookie({'name': 'reg_fb_gate', 'value': 'https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfeFKhDm07usUcZ9YEb4rMH51pFwUntUvs2vFky4hGvK_vhLgwBwhHCgXRs5qPysA4u0bEuAyvSLgk9Pkx3zH1oKQuiZvEN5KMx47BFUoc8Alg%26smuh%3D46396%26lh%3DAc9YslrRvglVPfu2', 'domain': '.facebook.com', 'path': '/',
    'httponly': false, 'secure': false, 'expires': (new Date()).getTime() + 3600 * 24 * 30
});

casper.start().thenOpen('https://www.facebook.com/', function() {
    this.fill('form#login_form', {// Логин
        email:  login,
        pass:  password
    }, true);
    this.click('#loginbutton > input');
    casper.wait(500);// Небольшое ожидание т.к. на их стороне AJAX валидация
});

casper.then(function() {
    var currentUrl = this.getCurrentUrl();
    var match1 = /(login.php)/.test(currentUrl);
    var match2 = /(checkpoint)/.test(currentUrl);
    if (match1 || match2) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.thenOpen('https://www.facebook.com/',function () {
    casper.wait(500);
    var id = /"ACCOUNT_ID":"([0-9]*)"/.exec(this.getHTML());// Вычленяем id аккаунта
    this.echo(JSON.stringify({
        status:'success',
        id:id[1]
    }));
});

casper.run();