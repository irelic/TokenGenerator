var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start('http://vk.com/', function() {
    this.fill('form#quick_login_form', {// Логин
        email:  login,
        pass:  password
    }, true);
    this.click('#quick_login_button');
    casper.wait(500);// Небольшое ожидание т.к. на их стороне AJAX валидация
});

casper.then(function() {
    var match = /(login.php)/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.thenOpen('http://vk.com/',function () {
    casper.wait(500);
    var id = /"user_id":([0-9]*),/.exec(this.getHTML());// Вычленяем id аккаунта
    this.echo(JSON.stringify({
        status:'success',
        id:id[1]
    }));
});

casper.run();