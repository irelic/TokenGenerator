var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

var response = {
    errors:[]
};
var link = '';
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
casper.on("page.error", function(msg, trace) {
    response.errors.push("Link: " + link + "; Page Error: " + msg);
});

// Принимаем параметры из запроса
var login = casper.cli.get("login");
var password = casper.cli.get("password");
var email = casper.cli.get("email");
var links = (casper.cli.get("links") + '').split(',');

casper.start('http://twitter.com/login/', function() {
    this.fill('#page-container > div > div.signin-wrapper > form', {// Логин
        'session[username_or_email]':  login,
        'session[password]':  password
    }, true);
    this.click('#page-container > div > div.signin-wrapper > form > div.clearfix > button');
    casper.wait(500);
});
casper.then(function() {
    var currentUrl = this.getCurrentUrl();
    var match = /error\?username_or_email=/.test(currentUrl);
    if (match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
    match = /login_challenge/.test(currentUrl);
    if (match) {
        this.fill('#login-challenge-form', {// Логин
            'challenge_response':  email
        }, true);
        this.click('#email_challenge_submit');
        casper.wait(500);
    }
});

casper.then(function () {
    links.forEach(function(item, i, arr) {
        link = item;
        casper.thenOpen(item,function(){// Переход по ссылке
            if(this.exists('#allow')){
                this.click('#allow');// Подтверждаем приложение
            }
            waitCallbackUrl();// Ожидаем пока не прогрузится до последней странице
        });
    });
});

casper.then(function () {
    this.echo(JSON.stringify(response));
});

casper.run();

function waitCallbackUrl(){
    casper.wait(500);
    if(! casper.getCurrentUrl().search(/(callback)/) ){
        waitCallbackUrl()
    }
}