var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

var apps_id = [];
// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start('http://twitter.com/login/', function() {
    this.fill('#page-container > div > div.signin-wrapper > form', {// Логин
        'session[username_or_email]':  login,
        'session[password]':  password
    }, true);
    this.click('#page-container > div > div.signin-wrapper > form > div.clearfix > button');
    casper.wait(500);
});
casper.then(function() {
    var match = /error\?username_or_email=/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.thenOpen('https://apps.twitter.com/', function () {
    // Собераем id всех приложений из ссылок
    apps_id = this.evaluate(function(){
        var links = document.querySelectorAll('#gaz-content-body > div.d-block.d-block-system.g-main > div > ul > li > div > div.app-details > h2 > a');
        var app_id = Array.prototype.map.call(links,function(link){
            var match = /app\/([0-9]*)\/show/.exec(link.getAttribute('href'));
            if (match[1] != undefined) {
                return match[1];
            } else {
                return '';
            }
        });
        return app_id;
    });
    if(apps_id.length == 0){
        this.exit();
    }
});

casper.then(function(){
    apps_id.forEach(function(element, index, array){// Переходим по каждому приложению и возвращаем ключи
        var appStatus;
        var consumerKey;
        var consumerSecret;
        casper.thenOpen('https://apps.twitter.com/app/' + element, function (){
            appStatus = this.fetchText('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.twitter-app > div.app-details > p.app-abuse > a');
        });
        casper.thenOpen('https://apps.twitter.com/app/' + element + '/keys', function (){
            consumerKey = this.fetchText('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.app-settings > div:nth-child(1) > span:nth-child(2)');
            consumerSecret = this.fetchText('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.app-settings > div:nth-child(2) > span:nth-child(2)');
        });
        casper.then(function (){
            this.echo(JSON.stringify({
                status:'success',
                appStatus:appStatus,
                consumerKey:consumerKey,
                consumerSecret:consumerSecret,
                idApp:element
            }));
        });
    })
});

casper.run();