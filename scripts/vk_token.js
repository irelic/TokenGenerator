var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages: false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();
var apps = casper.cli.get("apps");
var clients = (apps + '').split(',');

casper.start('http://vk.com/');

casper.then(function () {
    var firstLogin = true;
    clients.forEach(function (item, i, arr) {// Перебирает все приложения
        var response = {
            status: '',
            access_token: '',
            app_id: item,
            time: ''
        };
        var url = null;
        casper.thenOpen('https://oauth.vk.com/authorize?client_id=' + item + '&display=page&redirect_uri=' +
            'http://vk.com/&scope=notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,' +
            'groups,messages,notifications,stats,ads,offline&response_type=token&v=5.37',
            function () {
            // Если заходит первый раз, то логинится
            if (firstLogin) {
            // Обходим главную страницу, т.к. на главной зависает надолго
                this.fill('#login_submit', {
                    email: login,
                    pass: password
                }, true);
                this.wait(500);// Небольшое ожидание т.к. на их стороне AJAX валидация
                this.then(function() {
                    var match = /(&m=4&)/.test(this.getCurrentUrl());
                    if (match) {
                        this.echo(JSON.stringify({
                            status:'error',
                            message:'Account inactive'
                        }));
                        this.exit();
                    }
                });
                firstLogin = false;
            }
        });
        casper.then(function () {
            this.waitForSelector('#install_allow', function () {
                // Подтверждаем приложение
                this.click('#install_allow');
            });
        });
        casper.then(function () {
            casper.waitForSelector('body', function () {
                // Вычленяем из урла токен
                url = this.getCurrentUrl();
                var regexp = new RegExp('access_token=(.*)&expires_in.*');
                var match = regexp.exec(url);
                var access_token = match[1].toString();

                var date = new Date();

                response.time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +
                    date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                if (access_token.search(/([a-z0-9]*)/) >= 0) {
                    response.status = 'success';
                } else {
                    response.status = 'error';
                }
                response.access_token = access_token;
                // Выводим ответ
                this.echo(JSON.stringify(response));
            });
        });

    });
});

casper.run();