var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login");
var password = casper.cli.get("password");

casper.start('http://twitter.com/login/', function() {
    this.fill('#page-container > div > div.signin-wrapper > form', {// Логин
        'session[username_or_email]':  login,
        'session[password]':  password
    }, true);
    this.click('#page-container > div > div.signin-wrapper > form > div.clearfix > button');
    casper.wait(500);
});

casper.then(function() {
    var match = /error\?username_or_email=/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.then(function() {
    makeApplication();
});


casper.run();

function makeApplication()
{
    var response = {
        status:'success',
        message:'',
        consumer_key:'',
        consumer_secret:'',
        app_id:''
    };

    casper.thenOpen('https://apps.twitter.com/app/new', function () {// Заполняем данные и нажимаем создать приложение
        var string = makeRandom();
        this.click('#edit-tos-agreement');
        this.fill('#twitter-apps-create-form', {
            'name': string,
            'description': string,
            'url': 'http://' + string + '.ru',
            'callback_url': 'https://www.google.ru/'
        });
        this.click('#edit-submit');
        this.wait(1000);
    });
    casper.then(function () {
        if(this.exists('#messages > div > h4')){// Если выводит ошибку создания, то выходим из аккаунта
            response.message = this.fetchText('#messages > div > h4');
            this.exit();
        } else {// Переходим во вкладку с ключами
            casper.waitForSelector('#gaz-content-body > div.tabs > ul > li:nth-child(3) > a', function() {
                this.click('#gaz-content-body > div.tabs > ul > li:nth-child(3) > a');
                this.wait(500);
            });
        }
    });

    casper.then(function () {// Вычленяем ключи и выводим их
        casper.waitForSelector('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.app-settings', function () {
            response.consumer_key = this.fetchText('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.app-settings > div:nth-child(1) > span:nth-child(2)');
            response.consumer_secret = this.fetchText('#gaz-content-body > div.d-block.d-block-system.g-main > div > div.app-settings > div:nth-child(2) > span:nth-child(2)');
            var match = /app\/([0-9]*)\/keys/.exec(this.getCurrentUrl());
            if (match[1] != undefined) {
                response.app_id = match[1];
            }
            this.echo(JSON.stringify(response));
            makeApplication();
        });
    });
}

function makeRandom()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}