var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  true,         // The WebPage instance used by Casper will
        loadPlugins: true,         // use these settings
        userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5',
    }
});
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start('https://www.instagram.com/accounts/login/', function() {
    this.waitForSelector('form._rwf8p', function(){
        this.fill('form._rwf8p', {// Логин
            username: login,
            password: password
        }, true);
        this.click('form button._rz1lq');
    });
    casper.wait(500);
});

casper.then(function() {
    if(this.exists('form #slfErrorAlert')){
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});
casper.thenOpen('https://www.instagram.com/'+login,function() {
    var id = /"id":"([0-9]*)",/.exec(this.getHTML());// Вычленяем id аккаунта
    if(id[1] !== undefined) {
        this.echo(JSON.stringify({
            status:'success',
            id:id[1]
        }));
    } else {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
    }
});

casper.run();