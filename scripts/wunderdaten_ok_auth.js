var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start();

casper.open('http://wunderdaten.com/auth/ok/auth', {
    method: 'post',
    headers: {
        'Referer': 'http://token.dev/'
    }
});

casper.then(function() {
    this.waitForSelector('form', function(){
        this.fill('form', {// Логин
            'fr.email': login,
            'fr.password': password
        }, true);
        this.click('form > div.form-actions > input');
    });
    casper.wait(1000);
});

casper.then(function() {
    var match = /(&tokenId=)/.test(this.getCurrentUrl());
    if (!match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.then(function() {
    var url = this.getCurrentUrl();
    var regexp = new RegExp('&tokenId=([a-z0-9]*)&');
    var match = regexp.exec(url);
    var tokenId = match[1].toString();
    var redirectUrl = 'http://wunderdaten.com/auth/token/view?id='+tokenId;
    this.thenOpen(redirectUrl,function() {
        this.wait(1000);
        this.echo(this.getHTML('body', false));
    });
});

casper.run();