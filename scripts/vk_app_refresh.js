var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages: false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start('http://vk.com/');

casper.thenOpen('http://vk.com/apps?act=manage', function () {
    this.fill('form#login', {// Логин
        email: login,
        pass: password
    }, true);
    casper.wait(500);
});
casper.then(function () {
    var match = /(login.php)/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status: 'error',
            message: 'Account inactive'
        }));
        this.exit();
    }
});
casper.then(function () {
    var apps_id = this.evaluate(function () {
        var links = document.querySelectorAll('div.app_settings_actions.fl_r > a');// Выдераем ссылки приложений
        var ids = Array.prototype.map.call(links, function (link) {// Выдераем из ссылок приложений id
            var href = link.getAttribute('href');
            var match = /editapp\?id=(.*)/.exec(href);
            if (match[1] != undefined) {
                return match[1];
            }
        });
        return ids;
    });
    this.echo(JSON.stringify({
        status: 'success',
        appsId: apps_id
    }));
});

casper.run();