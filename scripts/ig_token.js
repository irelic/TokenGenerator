var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages: false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5'
    }
});
//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();
var userId = casper.cli.get("userId").toString();
var apps = casper.cli.get("apps");
var clients = (apps + '').split(',');

casper.start('https://www.instagram.com/accounts/login/', function () {
    this.waitForSelector('form._rwf8p', function(){
        this.fill('form._rwf8p', {// Логин
            username: login,
            password: password
        }, true);
        this.click('form button._rz1lq');
    });
    casper.wait(1000);
});

casper.thenOpen('https://www.instagram.com/developer/clients/manage/',function () {
    var match = /(register)/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status: 'error',
            message: 'Account inactive'
        }));
        this.exit();
    }
    casper.wait(500);
});

casper.eachThen(clients, function (client) {// Перебирает все приложения
    var response = {
        status: '',
        userId: userId,
        access_token: '',
        app_id: client.data,
        time: ''
    };
    var url = null;
    var redirectUrl = 'https://api.instagram.com/oauth/authorize/?client_id=' + client.data
        + '&redirect_uri=http://wunderdaten.com/auth/instagram/back&response_type=token';
    this.thenOpen(redirectUrl, function () {// Перехожим на генерацию токена
        this.wait(1000, function(){
            var error = this.fetchText('body');
            var isError = /^({"code":)/.test(error);
            if(isError) {// Если выдало код ошибки отправляем ошибку
                this.echo(JSON.stringify({
                    status:'error',
                    message: error
                }));
                this.exit();
            }

            var match = /(#access_token)/.test(this.getCurrentUrl());// Если токен уже был сгенерирован, то он не будет просить аутентификации

            if(!match){// Если новый токен то аутентифицируемся
                this.waitForSelector('input.button.confirm', function () {
                    this.click('input.button.confirm');
                });
            }
        });
        this.then(function () {
            // Вычленяем из урла токен
            url = this.getCurrentUrl();
            var regexp = new RegExp('access_token=(.*)');
            var match = regexp.exec(url);
            var access_token = match[1].toString();

            var date = new Date();

            response.time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +
                date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
            if (access_token !== undefined) {
                response.status = 'success';
            } else {
                response.status = 'error';
            }
            response.access_token = access_token;
            // Выводим ответ
            this.echo(JSON.stringify(response));
        });
    });
});

casper.run();