var casper = require('casper').create({
    //verbose: true,
    //logLevel: 'debug',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false,         // use these settings
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

//casper.on('remote.message', function(msg) {
//    this.echo('remote message caught: ' + msg);
//});
//
//casper.on("page.error", function(msg, trace) {
//    this.echo("Page Error: " + msg, "ERROR");
//});

// Принимаем параметры из запроса
var login = casper.cli.get("login").toString();
var password = casper.cli.get("password").toString();

casper.start('http://twitter.com/login/', function() {
    this.fill('#page-container > div > div.signin-wrapper > form', {// Логин
        'session[username_or_email]':  login,
        'session[password]':  password
    }, true);
    this.click('#page-container > div > div.signin-wrapper > form > div.clearfix > button');
    casper.wait(500);
});
casper.then(function() {
    var match = /error\?username_or_email=/.test(this.getCurrentUrl());
    if (match) {
        this.echo(JSON.stringify({
            status:'error',
            message:'Account inactive'
        }));
        this.exit();
    }
});

casper.then(function () {
    var match = /alt="Profile and settings" data-user-id="([0-9]*)"/.exec(this.getHTML());
    if (match[1] != undefined) {
        this.echo(JSON.stringify({
            status: 'success',
            id: match[1]
        }));
    }
    this.exit();
});

casper.run();