<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

    'ok' => [
        'Smart-Crowd-Developer' => [
            'login' => 'robot@smart-crowd.ru',
            'password' => 'QWEasdzxc321()'
        ],
        'analyzer2.prod' => [
            'login' => '79103951756',
            'password' => 'pUsRQGx41nPoBoju',
        ]
    ]

];
