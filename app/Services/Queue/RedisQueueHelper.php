<?php namespace App\Services\Queue;

use Illuminate\Support\Facades\Redis;

class RedisQueueHelper extends QueueHelper
{
    protected $redis;

    public function __construct()
    {
        $this->redis = Redis::connection();
    }

    public function allQueues()
    {
        $keys = $this->redis->keys('queues:*');
        $keys = array_filter($keys, function ($val) {
            return substr_count($val, ':') == 1;
        });
        $return = [];
        foreach ($keys as $key) {
            $return[$key] = [
                'size' => $this->redis->llen($key),
            ];
        }
        return $return;
    }
}