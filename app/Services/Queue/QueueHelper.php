<?php namespace App\Services\Queue;

abstract class QueueHelper implements QueueHelperInterface
{
    abstract public function allQueues();

    public function workers()
    {
        return trim(`ps aux|grep "artisan queue:listen" | grep -v grep|wc -l`);
    }

    public function totalSize()
    {
        $queues = $this->allQueues();
        $total = 0;
        foreach ($queues as $queue) {
            $total += $queue['size'];
        }
        return $total;
    }
}