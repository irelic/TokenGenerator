<?php namespace App\Services\Queue;

use Illuminate\Support\Facades\Queue;
use Pheanstalk\Pheanstalk;

class BeanstalkdQueueHelper extends QueueHelper
{
    /**
     * @var Pheanstalk
     */
    protected $pheanstalk;

    public function __construct()
    {
        $this->pheanstalk = Queue::connection('beanstalkd')->getPheanstalk();
    }

    public function allQueues()
    {
        $return = [];
        foreach ($this->pheanstalk->listTubes() as $tubeName) {
            $tubeStats = (array)$this->pheanstalk->statsTube($tubeName);
            $return[$tubeName] = [
                'size' => isset($tubeStats['current-jobs-ready']) ? $tubeStats['current-jobs-ready'] : 0,
            ];
        }
        return $return;
    }
}