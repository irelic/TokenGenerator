<?php namespace App\Services\Queue;

interface QueueHelperInterface
{
    public function allQueues();

    public function totalSize();

    public function workers();
}