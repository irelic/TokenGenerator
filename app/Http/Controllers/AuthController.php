<?php

namespace App\Http\Controllers;

use App\Account;
use App\Application;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Contracts\Factory;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Routing\Controller;
use Laravel\Socialite\One\TwitterProvider;
use League\OAuth1\Client\Server\Twitter as TwitterServer;

class AuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @param $appId
     * @param Request $request
     * @return Response
     */
    public function redirectToProvider($appId, Request $request)
    {
//        dd($this->getTwitterProvider($request, $appId));
        return $this->getTwitterProvider($request, $appId)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @param $appId
     * @param Request $request
     * @return Response
     */
    public function handleProviderCallback($appId, Request $request)
    {
        $user = $this->getTwitterProvider($request, $appId)->user();

        $application = Application::where('id', $appId)->first();

        $token = Token::where('account_id', $user->id)->where('app_id', $application->app_id)->first();

        Model::unguard();

        if ($token) {
            $token->token = $user->token;
            $token->secret = $user->tokenSecret;
            $token->update();
        } else {
            $token = new Token();
            $token->token = $user->token;
            $token->secret = $user->tokenSecret;
            $token->app_key = $application->app_key;
            $token->app_secret = $application->app_secret;
            $token->account_id = $user->id;
            $token->app_id = $application->app_id;
            $token->sn = 'tw';
            $token->save();
        }
    }

    private function getTwitterProvider($request, $appId)
    {
        $application = Application::find($appId);

        $provider = new TwitterProvider(
            $request, new TwitterServer([
                'identifier' => $application->app_key,
                'secret' => $application->app_secret,
                'callback_uri' => Config::get('queue.link') . 'auth/twitter/' . $appId . '/callback',
            ])
        );

        return $provider;
    }
}