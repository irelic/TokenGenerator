<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class AccountsController extends Controller
{
    public function getOkToken($appId)
    {
        $auth = config('services.ok.' . $appId);

        exec('casperjs '.base_path().'/scripts/wunderdaten_ok_auth.js --login="' . array_get($auth, 'login') . '" --password="' .
            array_get($auth, 'password') . '" --ssl-protocol=any', $response);
        if($jsonResponse = json_decode($response[0])) {
            echo $response[0];
        } else {
            echo json_encode(['status' => 'error', 'message' => "Error processing response"]);
        }
    }

}