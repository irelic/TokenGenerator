<?php

namespace App;

use App\Application;
use App\Token;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    protected $fillable = ['login','password','account_id','account_secret','sn','email','phone','status'];

    static public function getFreeAccounts()
    {
        $accounts = Account::all();
        $freeAccounts = [];
        foreach ($accounts as $account) {
            if (count(Token::getFreeAccs($account)) > 0) {
                $freeAccounts[] = $account;
            }
        }
        return $freeAccounts;
    }

    public function isFilled()
    {
        $countApplication = Application::where('sn', $this->sn)->count();
        $countTokenAccount = Token::where('sn', $this->sn)->where('account_id', $this->account_id)->count();
        if ($countTokenAccount < $countApplication) {
            return false;
        }

        return true;
    }

    public function inactive()
    {
        $this->touch();
        $this->status = 'inactive';
        $this->update();
    }

    public function active()
    {
        $this->touch();
        $this->status = '';
        $this->update();
    }

    static public function whereSn($sn)
    {
        return self::where('sn', $sn)->get();
    }
}
