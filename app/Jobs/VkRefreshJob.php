<?php

namespace App\Jobs;

use App\Application;

class VkRefreshJob extends RefreshJob
{
    public $pathScript = 'scripts/vk_app_refresh.js';

    public function parseResponse(){

        $responseParams = json_decode($this->scriptResponse[0]);

        if($responseParams->status == 'success') {
            foreach ($responseParams->appsId as $app_id) {
                if (!(Application::where('app_id', $app_id)->first())) {
                    $this->saveApplication($app_id);
                }
            }
        } else {
            if($responseParams->message == 'Account inactive') {
                $this->account->inactive();
            }
        }
    }

    public function saveApplication($app_id){
        $application = new Application();
        $application->fill([
            'app_id' => $app_id,
            'account_id' => $this->account->account_id,
            'sn' => $this->account->sn,
        ]);
        $application->save();
    }
}
