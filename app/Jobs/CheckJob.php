<?php

namespace App\Jobs;

use App\Token;
use App\Application;
use Codebird\Codebird;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

class CheckJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $tokenId;
    public $token;

    /**
     * Create a new job instance.
     *
     * @param $tokenId
     */
    public function __construct($tokenId)
    {
        $this->tokenId = $tokenId;
    }

    public function getToken()
    {
        if($token = Token::find($this->tokenId)){
            $this->token = $token;
        } else {
            die;
        }
    }

    public function changeStatus($status)
    {
        if ($status) {
            $this->token->inactive();
        } else {
            $this->token->active();
        }
    }
}
