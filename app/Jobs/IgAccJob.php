<?php

namespace App\Jobs;

use App\Account;
use App\Jobs\JobFactory;
use App\Token;
use App\Application;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

class IgAccJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    public $data;

    /**
     * Create a new job instance.
     *
     * @param Account $account
     * @internal param Account $token
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = new Account();
        $account->fill([
            'login' => $this->data[0],
            'password' => $this->data[1],
            'account_id' => $this->data[2],
            'account_secret' => $this->data[3],
            'sn' => $this->data[4],
            'email' => $this->data[5],
            'phone' => $this->data[6]
        ]);

        if (!empty($account->email) && Account::where('email', $account->email)->where('sn', $account->sn)->first()) {
            return;
        }

        if (empty($account->account_id)) {
            exec('casperjs scripts/ig_acc_refresh.js --login="' . $account->login . '" --password="' .
                $account->password . '" --ssl-protocol=any', $response);
            $responseParams = json_decode($response[0]);

            if ($responseParams && $responseParams->status == 'success') {
                $account->account_id = $responseParams->id;
            } else {
                return;
            }
        }

        if (Account::where('account_id', $account->account_id)->where('sn', $account->sn)->first()) {
            return;
        }

        $account->save();
    }
}
