<?php

namespace App\Jobs;

use App\Account;
use App\Jobs;
use App\Token;

class JobFactory
{
    protected $sn;

    public function __construct($sn)
    {
        $this->sn = $sn;
    }

    /**
     * @param String $accountId
     * @return Job
     * @throws \Exception
     */
    public function getAppJob($accountId)
    {
        $jobName = $this->buildClassName('AppJob');
        if (!class_exists($jobName)) {
            throw new \Exception();
        }

        return new $jobName($accountId);
    }

    public function getTokenJob($accountId)
    {
        $jobName = $this->buildClassName('TokenJob');
        if (!class_exists($jobName)) {
            throw new \Exception();
        }

        return new $jobName($accountId);
    }

    public function getCheckJob($tokenId)
    {
        $jobName = $this->buildClassName('CheckJob');
        if (!class_exists($jobName)) {
            throw new \Exception();
        }

        return new $jobName($tokenId);
    }

    public function getAccJob(array $data)
    {
        $jobName = $this->buildClassName('AccJob');
        if (!class_exists($jobName)) {
            throw new \Exception();
        }

        return new $jobName($data);
    }

    public function getRefreshAccJob(Account $account)
    {
        $jobName = $this->buildClassName('RefreshJob');
        if (!class_exists($jobName)) {
            throw new \Exception();
        }

        return new $jobName($account);
    }

    private function buildClassName($class)
    {
        return 'App\\Jobs\\' . ucfirst($this->sn) . $class;
    }
}
