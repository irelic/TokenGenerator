<?php

namespace App\Jobs;

use Codebird\Codebird;

class TwCheckJob extends CheckJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getToken();

        $application = $this->token->application;
        Codebird::setConsumerKey($application->app_key, $application->app_secret); // static, see 'Using multiple Codebird instances'

        $cb = Codebird::getInstance();

        $cb->setToken($this->token->token, $this->token->secret);
        $reply = (array)$cb->statuses_homeTimeline();
        if (isset($reply['httpstatus'])) {
            $this->changeStatus($reply['httpstatus'] < '200' || $reply['httpstatus'] > '299');
        }
    }
}
