<?php

namespace App\Jobs;

use App\Account;
use App\Token;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class TokenJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $accountId;
    public $freeApps;
    public $account;

    /**
     * Create a new job instance.
     *
     * @param $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    public function getFreeApps()
    {
        $this->account = Account::find($this->accountId);
        if(!$this->account) {
            $this->delete();
            die;
        }
        $this->freeApps = Token::getFreeApps($this->account->sn, $this->account->account_id);
        if(!count($this->freeApps)) {
            $this->delete();
            die;
        }
    }
}
