<?php

namespace App\Jobs;

use Codebird\Codebird;

class IgCheckJob extends CheckJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getToken();

        $r = 'https://api.instagram.com/v1/users/self/?access_token=' . $this->token->token;
        $chp = curl_init($r);
        curl_setopt($chp, CURLOPT_HEADER, 0);
        curl_setopt($chp, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chp, CURLOPT_SSL_VERIFYPEER, 0);
        $resultp = curl_exec($chp);
        curl_close($chp);
        $jsonResult = json_decode($resultp);

        $this->changeStatus($jsonResult->meta->code != '200');
    }

}
