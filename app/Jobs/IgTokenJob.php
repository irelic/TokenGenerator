<?php

namespace App\Jobs;

use App\Token;

class IgTokenJob extends TokenJob
{

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getFreeApps();
        $appsArray = $this->freeApps->lists('app_id')->toArray();
        $apps = implode(',', $appsArray);
        exec('casperjs scripts/ig_token.js --login="' . $this->account->login . '" --password="' . $this->account->password .
            '" --apps="' . $apps . '" --userId="' . $this->account->account_id . '" --ssl-protocol=any', $response);

        foreach ($response as $num => $responseString) {
            if (preg_match('/^{.*}$/', $responseString)) {
                $createdToken = json_decode($responseString);

                if (isset($createdToken->status) && isset($createdToken->access_token) &&
                    isset($createdToken->app_id) && isset($createdToken->time)
                ) {

                    if ($createdToken->status == 'success') {
                        $token = new Token();
                        $token->fill([
                            'token' => $createdToken->access_token,
                            'account_id' => $createdToken->userId,
                            'app_id' => $createdToken->app_id,
                            'sn' => $this->account->sn,
                        ]);
                        $token->save();
                    } else {
                        file_put_contents('scripts/ig_tokens.log', date('Y-m-d H:i:s') . '; ERROR:' .
                            $createdToken->message . PHP_EOL, FILE_APPEND);
                    }
                }
            } else {
                file_put_contents('scripts/ig_tokens.log', date('Y-m-d H:i:s') . '; FATAL ERROR:' .
                    $responseString . PHP_EOL, FILE_APPEND);
            }

        }
    }

}
