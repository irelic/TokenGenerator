<?php

namespace App\Jobs;

use App\Account;
use App\Token;
use App\Application;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

abstract class RefreshJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $account;
    public $pathScript;
    public $scriptResponse;

    /**
     * Create a new job instance.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->runScript();
        $this->parseResponse();
    }

    public function runScript(){
        exec('casperjs '.$this->pathScript.' --login="' . $this->account->login . '" --password="' .
            $this->account->password . '" --ssl-protocol=any', $response);
        if (empty($response)) {die;}
        $this->scriptResponse = $response;
    }

    abstract function parseResponse();
}
