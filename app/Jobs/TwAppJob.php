<?php

namespace App\Jobs;

use App\Account;
use App\Application;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class TwAppJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $accountId;

    /**
     * Create a new job instance.
     *
     * @param $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = Account::find($this->accountId);
        if(!$account) return;

        exec('casperjs scripts/tw_app.js --login="' . $account->login . '" --password="' . $account->password .
            '" --ssl-protocol=any', $response);

        if (!empty($response)) {
            foreach ($response as $num => $responseString) {

                $responseParams = json_decode($responseString);

                if ($responseParams && $responseParams->status == 'success') {
                    if ( !(Application::where('app_id', $responseParams->app_id)->where('sn', 'tw')->first()) ) {
                        $application = new Application();
                        $application->fill([
                            'app_id' => $responseParams->app_id,
                            'app_key' => $responseParams->consumer_key,
                            'app_secret' => $responseParams->consumer_secret,
                            'account_id' => $account->account_id,
                            'sn' => $account->sn,
                        ]);
                        $application->save();
                    }
                } else {
                    if($responseParams->message == 'Account inactive') {
                        $account->inactive();
                    }
                    file_put_contents('scripts/tw_application.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login .
                        ' FATAL ERROR:' . implode('; ', $responseString) . PHP_EOL, FILE_APPEND);
                }

            }
        }
        $account->touch();
    }
}
