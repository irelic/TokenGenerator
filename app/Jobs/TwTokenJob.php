<?php

namespace App\Jobs;

use App\Account;
use App\Token;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class TwTokenJob extends TokenJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getFreeApps();

        $appsId = $this->freeApps->lists('id')->toArray();

        $links = array_map(function ($id) {
            return Config::get('queue.link') . 'auth/twitter/' . $id;
        }, $appsId);

        $links = implode(',', $links);
        exec('casperjs scripts/tw_token.js --login="' . $this->account->login . '" --password="' . $this->account->password . '" --email="' . $this->account->email . '" --links="' . $links . '" --ssl-protocol=any', $casperResponse);
        if (!empty($casperResponse)) {
            $response = json_decode($casperResponse[0]);
            if (isset($response->errors)) {
                foreach ($response->errors as $error) {
                    file_put_contents('scripts/tw_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $this->account->login . ' ERROR:' . $error . PHP_EOL, FILE_APPEND);
                }
            }
            if (isset($response->status) && $response->status == "error") {
                if($response->message == 'Account inactive') {
                    $this->account->inactive();
                }
            }
            file_put_contents('scripts/tw_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $this->account->login . ' FATAL ERROR:' . implode('; ', $casperResponse) . PHP_EOL, FILE_APPEND);
        }
    }
}
