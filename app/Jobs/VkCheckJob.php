<?php

namespace App\Jobs;

class VkCheckJob extends CheckJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getToken();

        $r = 'https://api.vk.com/method/users.get?user_id=' . $this->token->account_id .
            '&v=5.37&access_token=' . $this->token->token;
        $chp = curl_init($r);
        curl_setopt($chp, CURLOPT_HEADER, 0);
        curl_setopt($chp, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chp, CURLOPT_SSL_VERIFYPEER, 0);
        $resultp = curl_exec($chp);
        curl_close($chp);

        $this->changeStatus(
            preg_match('/"error_code":([0-9]*),/', $resultp, $matche) &&
            ($matche[1] == 5 || $matche[1] == 10));
    }
}
