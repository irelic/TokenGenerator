<?php

namespace App\Jobs;

use App\Application;

class TwRefreshJob extends RefreshJob
{
    public $pathScript = 'scripts/tw_app_refresh.js';

    public function parseResponse()
    {
        foreach ($this->scriptResponse as $num => $responseString) {

            $responseParams = json_decode($responseString);

            $app = Application::where('app_id', $responseParams->idApp)->where('sn', 'tw')->first();

            if (!$app && $responseParams->status == 'success') {
                $this->saveApplication($responseParams);
            } else {
                if ($responseParams->status == 'success') {
                    $app->status = $responseParams->appStatus;
                    $app->save();
                } else {
                    if ($responseParams->message == 'Account inactive') {
                        $this->account->inactive();
                    }
                    file_put_contents('scripts/tw_application_refresh.log', date('Y-m-d H:i:s') . ' ' .
                        implode('; ', $responseParams) . PHP_EOL, FILE_APPEND);

                }
            }

        }
    }

    public function saveApplication($params)
    {
        $application = new Application();
        $application->fill([
            'app_id' => $params->idApp,
            'app_key' => $params->consumerKey,
            'app_secret' => $params->consumerSecret,
            'account_id' => $this->account->account_id,
            'sn' => $this->account->sn,
        ]);
        $application->save();
    }
}
