<?php

namespace App\Jobs;

use App\Account;
use App\Token;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

class VkTokenJob extends TokenJob
{

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getFreeApps();
        $appsArray = $this->freeApps->lists('app_id')->toArray();
        $apps = implode(',', $appsArray);
        exec('casperjs scripts/vk_token.js --login="' . $this->account->login . '" --password="' .
            $this->account->password . '" --apps="' . $apps . '" --ssl-protocol=any', $response);
        foreach ($response as $num => $responseString) {
            $log = 'ACC:' . $this->account->login . '; RESPONSE:' . $responseString . ';';
            file_put_contents('scripts/vk_tokens.log', $log . PHP_EOL, FILE_APPEND);

            $responseParams = json_decode($responseString);
            if ($responseParams && $responseParams->status == "success") {
                $token = new Token();
                $token->fill([
                    'token' => $responseParams->access_token,
                    'account_id' => $this->account->account_id,
                    'app_id' => $responseParams->app_id,
                    'sn' => $this->account->sn,
                ]);
                $token->save();
            } else {

            }

        }
    }
}
