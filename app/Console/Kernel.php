<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\VkTokensCommand::class,
        \App\Console\Commands\IgTokensCommand::class,
        \App\Console\Commands\ApplicationsRefreshCommand::class,
        \App\Console\Commands\TwApplicationCommand::class,
        \App\Console\Commands\TwTokenCommand::class,
        \App\Console\Commands\TwApplicationRefreshCommand::class,
        \App\Console\Commands\TestCommand::class,
        \App\Console\Commands\TokenGenerateCommand::class,
        \App\Console\Commands\ApplicationGenerateCommand::class,
        \App\Console\Commands\TokenCheckCommand::class,
        \App\Console\Commands\RunTokenQueues::class,
        \App\Console\Commands\AccountAddCommand::class,
        \App\Console\Commands\UnloadingCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    }
}
