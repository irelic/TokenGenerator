<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Queue\RedisQueueHelper;
use Illuminate\Support\Facades\Config;
use Symfony\Component\Process\Process;

class RunTokenQueues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:queues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $redis = new RedisQueueHelper;
        $queues = $redis->allQueues();

        $this->adjustQueues();

//        $queueTwGenerateExist = (array_key_exists('queues:twtoken', $queues) || array_key_exists('queues:twapp', $queues));
        $queueCheckExist = (array_key_exists('queues:twcheck', $queues)
            || array_key_exists('queues:vkcheck', $queues)
            || array_key_exists('queues:igcheck', $queues)
        );
//        $queueVkGenerateExist = array_key_exists('queues:vktoken', $queues);

//        if (!$queueTwGenerateExist) {
//            $this->call('token:generate', ['--sn' => 'tw']);
//            $this->call('application:generate', ['--sn' => 'tw']);
//        }

        if (!$queueCheckExist) {
            $this->call('token:check', ['--sn' => 'vk']);
            $this->call('token:check', ['--sn' => 'tw']);
            $this->call('token:check', ['--sn' => 'ig']);
        }

//        if (!$queueVkGenerateExist) {
//            $this->call('token:generate', ['--sn' => 'vk']);
//        }

    }

    private function adjustQueues()
    {
//        $countGenerateQueues = $this->countQueues(
//            'artisan queue:listen --queue=vktoken,igtoken,twtoken,twapp');
        $countCheckQueues = $this->countQueues(
            'artisan queue:listen --queue=vkacc,igacc,vkrefresh,twacc,twrefresh,igcheck,vkcheck,twcheck');

//        $requiredGenerateQueues = Config::get('queue.countGenerate') - $countGenerateQueues;
        $requiredCheckQueues = Config::get('queue.countCheck') - $countCheckQueues;

//        while ($requiredGenerateQueues > 0) {
//            $commandGenerateWorker = new Process(
//                'php artisan queue:listen --queue=vktoken,igtoken,twtoken,twapp --timeout=0 --sleep=900  &>/dev/null &');
//            $commandGenerateWorker->setTty(true)->disableOutput()->start();
//
//            $requiredGenerateQueues--;
//        }

        while ($requiredCheckQueues > 0) {
            $commandCheckWorker = new Process(
                'php artisan queue:listen --queue=vkacc,igacc,vkrefresh,twacc,twrefresh,igcheck,vkcheck,twcheck --timeout=0 --sleep=900  &>/dev/null &');
            $commandCheckWorker->setTty(true)->disableOutput()->start();
            $requiredCheckQueues--;
        }

    }

    private function countQueues($nameQueue)
    {
        $commandCountGenerateWorkers = new Process(
            'ps aux|grep "'.$nameQueue.'" | grep -v grep|wc -l');
        $commandCountGenerateWorkers->run();
        return trim($commandCountGenerateWorkers->getOutput());

    }
}
