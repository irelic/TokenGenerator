<?php

namespace App\Console\Commands;

use App\Account;
use App\Jobs\JobFactory;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TokenGenerateCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:generate
        {--sn=  : Social network. vk or tw}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sn = $this->option('sn');

        ini_set('memory_limit', '1024M');

        $accounts = Account::where('status', '<>', 'inactive')->whereSn($sn)->get();

        $jobFactory = new JobFactory($sn);

        foreach ($accounts as $account) {
            if (!$account->isFilled()) {
                $this->dispatch($jobFactory->getTokenJob($account->id)->onQueue($sn . 'token'));
            }
        }
    }

}
