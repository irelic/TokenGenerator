<?php

namespace App\Console\Commands;

use App\Account;
use App\Application;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class TwApplicationRefreshCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:twapprefresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();
        $accounts = Account::where('sn', 'tw')->whereNotNull('phone')->get();
        foreach ($accounts as $account) {
            exec('casperjs scripts/tw_app_refresh.js --login="' . $account->login . '" --password="' . $account->password .
                '" --ssl-protocol=any', $response);
            file_put_contents('scripts/tw_application_refresh.log', date('Y-m-d H:i:s') . ' ' . implode('; ', $response) .
                PHP_EOL, FILE_APPEND);
            if (!empty($response)) {
                foreach ($response as $num => $responseString) {
                    $accesses = explode(',', $responseString);
                    if (!empty($accesses[0]) && $accesses[1]) {
                        $app = Application::where('app_id', $accesses[1])->where('sn', 'tw')->first();
                        $app->status = $accesses[0];
                        $app->save();
                    }

//                    if ($accesses[0] && $accesses[1] && $accesses[2] &&
//                        !(Application::where('app_id', $accesses[2])->where('sn', 'tw')->first())
//                    ) {
//                        $application = new Application();
//                        $application->fill([
//                            'app_id' => $accesses[2],
//                            'app_key' => $accesses[0],
//                            'app_secret' => $accesses[1],
//                            'account_id' => $account->account_id,
//                            'sn' => $account->sn,
//                        ]);
//                        $application->save();
//                    }
                }
            }
            unset($response);
        }
    }
}
