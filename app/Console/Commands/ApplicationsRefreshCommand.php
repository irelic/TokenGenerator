<?php

namespace App\Console\Commands;

use App\Account;
use App\Application;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use App\Jobs\JobFactory;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ApplicationsRefreshCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:apprefresh
        {--sn=  : Social network. tw or vk}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sn = $this->option('sn');

        ini_set('memory_limit', '1024M');

        $accounts = Account::where('sn',$sn)->get();

        $jobFactory = new JobFactory($sn);

        foreach ($accounts as $account) {
            $this->dispatch($jobFactory->getRefreshAccJob($account)->onQueue($sn . 'refresh'));
        }
    }
}
