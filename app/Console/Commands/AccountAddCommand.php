<?php

namespace App\Console\Commands;

use App\Jobs\JobFactory;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AccountAddCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:add
        {--file=  : Path to the CSV file with accounts}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = base_path($this->option('file'));

        ini_set('memory_limit', '1024M');

        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if(count($data) == 7 && !empty($data[0]) && !empty($data[1]) && !empty($data[4]) ){
                    $jobFactory = new JobFactory($data[4]);

                    $this->dispatch($jobFactory->getAccJob($data)->onQueue($data[4].'acc'));
                }

            }
            fclose($handle);
        }

    }

}
