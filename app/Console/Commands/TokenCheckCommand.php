<?php

namespace App\Console\Commands;

use App\Jobs\JobFactory;
use App\Token;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TokenCheckCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:check
        {--sn=  : Social network. vk, tw, ig}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sn = $this->option('sn');

        ini_set('memory_limit', '1024M');

        $tokensId = Token::where('sn', $sn)->orderBy('updated_at', 'asc')->lists('id');

        $jobFactory = new JobFactory($sn);

        foreach ($tokensId as $tokenId) {
            $this->dispatch($jobFactory->getCheckJob($tokenId)->onQueue($sn . 'check'));
        }
    }

}
