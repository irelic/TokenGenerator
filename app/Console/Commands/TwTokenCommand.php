<?php

namespace App\Console\Commands;

use App\Account;
use App\Token;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Routing\Controller;

class TwTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:twtoken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = Account::where('sn', 'tw')->get();

        foreach ($accounts as $account) {
            $freeApps = Token::getFreeApps('tw', $account->account_id);
            if (count($freeApps)) {
                $appsId = $freeApps->lists('id')->toArray();

                $links = array_map(function ($id) {
                    return Config::get('queue.link') . 'auth/twitter/' . $id;
                }, $appsId);
                $links = implode(',', $links);
                exec('casperjs scripts/tw_token.js --login="' . $account->login . '" --password="' . $account->password . '" --links="' . $links . '" --ssl-protocol=any', $response);
                if (preg_match('/^{.*}$/', $response[0])) {
                    $response = json_decode($response[0]);
                    if (isset($response->errors)) {
                        foreach ($response->errors as $error) {
                            file_put_contents('scripts/tw_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login . ' ERROR:' . $error . PHP_EOL, FILE_APPEND);
                        }
                    }
                } else {
                    file_put_contents('scripts/tw_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login . ' FATAL ERROR:' . implode('; ', $response) . PHP_EOL, FILE_APPEND);
                }
            }
            unset($response);
        }
    }

}
