<?php

namespace App\Console\Commands;

use App\Account;
use App\Application;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class TwApplicationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:twapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();
        $accounts = Account::where('sn', 'tw')->where('phone', '<>', '')->where('updated_at', '<', (new Carbon)->subHour(25))->get();
        foreach ($accounts as $account) {
            exec('casperjs scripts/tw_app.js --login="' . $account->login . '" --password="' . $account->password .
                '" --ssl-protocol=any', $response);
            file_put_contents('scripts/tw_application.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login . ' ' .
                implode('; ', $response) . PHP_EOL, FILE_APPEND);
            if (!empty($response)) {
                foreach ($response as $num => $responseString) {

                    if (preg_match('/^{.*}$/', $responseString)) {
                        $createdApp = json_decode($responseString);
                        if (isset($createdApp->consumer_key) &&
                            isset($createdApp->consumer_secret) &&
                            isset($createdApp->app_id) &&
                            !(Application::where('app_id', $createdApp->app_id)->where('sn', 'tw')->first())
                        ) {
                            $application = new Application();
                            $application->fill([
                                'app_id' => $createdApp->app_id,
                                'app_key' => $createdApp->consumer_key,
                                'app_secret' => $createdApp->consumer_secret,
                                'account_id' => $account->account_id,
                                'sn' => $account->sn,
                            ]);
                            $application->save();
                        }
                    } else {
                        file_put_contents('scripts/tw_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login .
                            ' FATAL ERROR:' . implode('; ', $responseString) . PHP_EOL, FILE_APPEND);
                    }

                }
            }
            $account->touch();
            unset($response);
        }
    }
}
