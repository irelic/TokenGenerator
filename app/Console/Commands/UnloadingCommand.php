<?php

namespace App\Console\Commands;

use App\Account;
use App\Application;
use App\Services\Queue\RedisQueueHelper;
use Illuminate\Console\Command;
use Codebird;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Routing\Controller;

class UnloadingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unloading
        {--sn=  : Social network. tw or vk}
        {--count=  : count tokens}
        {--tag=  : CSV filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command unloading tokens in file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sn = $this->option('sn');
        $count = $this->option('count');
        $tag = $this->option('tag');

        if ($tag && $count && $sn) {
            $templateExport = ['token', 'secret', 'app_key', 'app_secret'];
            if ($sn == 'vk') {
                $templateExport = ['token'];
            }
            if ($sn == 'ig') {
                $templateExport = ['token', 'account_id'];
            }
            $date = date('Y-m-d_H-i-s');
            \App\Token::where('sn', $sn)
                ->where('status', '<>', 'inactive')
                ->where('mark_unloading', '')
                ->limit(intval($count))
                ->update(['mark_unloading' => $tag . "_" . $date]);
            $tokens = \App\Token::where('sn', $sn)
                ->where('status', '<>', 'inactive')
                ->where(['mark_unloading' => $tag . "_" . $date])
                ->get($templateExport)
                ->toArray();
            $handle = fopen($tag . ".csv", 'w');
            fputcsv($handle, $templateExport);
            foreach ($tokens as $token) {
                fputcsv($handle, $token);
            }
            fclose($handle);
        }

    }

}
