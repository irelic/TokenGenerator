<?php

namespace App\Console\Commands;

use App\Account;
use App\Jobs\JobFactory;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ApplicationGenerateCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:generate
        {--sn=  : Social network. Only tw}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sn = $this->option('sn');

        $accountsId = Account::where('sn', $sn)
            ->where('phone', '<>', '')
            ->where('updated_at', '<', (new Carbon)->subHour(24))
            ->orderBy('updated_at', 'asc')->take(1)
            ->lists('id');

        $jobFactory = new JobFactory($sn);
        foreach ($accountsId as $accountId) {
            $this->dispatch($jobFactory->getAppJob($accountId)->onQueue($sn . 'app'));
        }
    }

}
