<?php

namespace App\Console\Commands;

use App\Services\Queue\RedisQueueHelper;
use Illuminate\Console\Command;
use Codebird\Codebird;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test
        {--option=  : Path to the CSV file with accounts}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $option = $this->option('option');

        switch ($option) {
            case 'TwTokenCount':
                var_dump(\App\Token::where('sn', 'tw')->count());
                break;
            case 'queue':
                $redis = new RedisQueueHelper;
                var_dump($redis->allQueues());
                break;
            case 'TwFreeToken':
                $tokens = \App\Token::where('sn', 'tw')->where('status', '<>', 'inactive')->where('mark_unloading', '')->count();
                var_dump($tokens);
                break;
        }
    }

}
