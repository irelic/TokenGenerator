<?php

namespace App\Console\Commands;

use App\Account;
use App\Token;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class IgTokensCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:igtokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();
        $accounts = Account::where('sn', 'ig')->get();
        foreach ($accounts as $account) {
            $freeApps = Token::getFreeApps('ig', $account->account_id);
            if ($freeApps) {
                $appsArray = $freeApps->lists('app_id')->toArray();
                $apps = implode(',', $appsArray);
                exec('casperjs scripts/ig_token.js --login="' . $account->login . '" --password="' . $account->password .
                    '" --apps="' . $apps . '" --userId="' . $account->account_id . '" --ssl-protocol=any', $response);
                foreach ($response as $num => $responseString) {

                    if (preg_match('/^{.*}$/', $responseString)) {
                        $createdToken = json_decode($responseString);

                        if (isset($createdToken->status) && isset($createdToken->access_token) &&
                            isset($createdToken->app_id) && isset($createdToken->time)
                        ) {

                            if ($createdToken->status == 'success') {
                                $token = new Token();
                                $token->fill([
                                    'token' => $createdToken->access_token,
                                    'account_id' => $createdToken->userId,
                                    'app_id' => $createdToken->app_id,
                                    'sn' => $account->sn,
                                ]);
                                $token->save();
                            } else {
                                file_put_contents('scripts/ig_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login .
                                    ' ERROR:' . $createdToken->message . PHP_EOL, FILE_APPEND);
                            }
                        }
                    } else {
                        file_put_contents('scripts/ig_tokens.log', date('Y-m-d H:i:s') . '; ACC:' . $account->login .
                            ' FATAL ERROR:' . $responseString . PHP_EOL, FILE_APPEND);
                    }

                }
                unset($response);
            }
        }
    }
}
