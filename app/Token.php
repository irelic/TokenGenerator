<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = ['token','secret','account_id','app_id','sn','status','app_key','app_secret','mark_unloading'];

    public function application()
    {
        return $this->hasOne('App\Application', 'app_id', 'app_id');
    }

    public function account()
    {
        return $this->hasOne('App\Account', 'account_id', 'account_id');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tokens';

    static public function getFreeApps($sn, $accountId)
    {
        $account = \App\Account::where('sn', $sn)->where('account_id', $accountId)->first();
        if ($account) {
            $allApplicationsId = \App\Application::where('sn', $sn)->where('status', '<>', 'Suspended')->lists('app_id')->toArray();
            $userApplicationsId = \App\Token::where('sn', $sn)->where('account_id', $accountId)->lists('app_id')->toArray();
            $freeApplicationsId = array_values(array_diff($allApplicationsId, $userApplicationsId));
            $freeApplications = \App\Application::whereIn('app_id', $freeApplicationsId)->get();
            return $freeApplications;
        }
    }

    static public function getFreeAccs($account)
    {
        if ($account) {
            $allApplicationsId = \App\Application::where('sn', $account->sn)->where('status', '<>', 'Suspended')->lists('id')->toArray();
            $userApplicationsId = \App\Token::where('sn', $account->sn)->where('account_id', $account->account_id)->lists('app_id')->toArray();
            $freeApplicationsId = array_values(array_diff($allApplicationsId, $userApplicationsId));
            $freeApplications = \App\Application::whereIn('app_id', $freeApplicationsId)->get();
            return $freeApplications;
        }
    }

    public function inactive()
    {
        if ($this->status == 'inactive') {
//            $this->delete();
        } else {
            $this->touch();
            $this->status = 'inactive';
            $this->update();
        }
    }

    public function active()
    {
        $this->touch();
        $this->status = 'active';
        $this->update();
    }
}
