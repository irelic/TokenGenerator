<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applications';

    protected $fillable = ['app_id','app_key','account_id','sn','app_secret'];

    static public function whereSn($sn)
    {
        return self::where('sn', $sn)->get();
    }
}