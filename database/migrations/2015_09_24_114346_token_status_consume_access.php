<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TokenStatusConsumeAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tokens', function ($table) {
            $table->string('status');
            $table->string('app_key');
            $table->string('app_secret');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tokens', function ($table) {
            $table->dropColumn('status');
            $table->dropColumn('app_key');
            $table->dropColumn('app_secret');
        });
    }
}
