<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VkTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login');
            $table->string('password');
            $table->string('account_id');
            $table->string('account_secret');
            $table->string('sn');
            $table->timestamps();
        });

        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_id');
            $table->string('app_key');
            $table->string('account_id');
            $table->string('sn');
            $table->timestamps();
        });

        Schema::create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->string('secret');
            $table->string('account_id');
            $table->string('app_id');
            $table->string('sn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
        Schema::drop('applications');
        Schema::drop('tokens');
    }
}
