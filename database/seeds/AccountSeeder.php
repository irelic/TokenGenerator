<?php

use App\Account;
use App\Jobs\JobFactory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AccountSeeder extends Seeder
{
    use DispatchesJobs;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = app_path('../database/seeds/data/account.csv');
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if(count($data) == 7 && !empty($data[0]) && !empty($data[1]) && !empty($data[4]) ){
                    $jobFactory = new JobFactory($data[4]);

                    $this->dispatch($jobFactory->getAccJob($data)->onQueue($data[4].'acc'));
                }
            }
            fclose($handle);
        }
    }
}
