<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = app_path('../database/seeds/data/token.csv');

        ini_set('memory_limit', '1024M');

        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if(count($data) == 11 && !empty($data[0]) && !empty($data[1]) && !empty($data[4]) &&
                    !\App\Token::where('sn', $data[4])->where('account_id',$data[2])->where('app_id',$data[3])->first()){
                    $token = new \App\Token();
                    $token->token = $data[0];
                    $token->secret = $data[1];
                    $token->account_id = $data[2];
                    $token->app_id = $data[3];
                    $token->sn = $data[4];
                    $token->status = $data[7];
                    $token->app_key = $data[8];
                    $token->app_secret = $data[9];
                    $token->mark_unloading = $data[10];
                    $token->save();
                }

            }
            fclose($handle);
        }
    }
}
