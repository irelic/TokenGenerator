<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = app_path('../database/seeds/data/application.csv');

        ini_set('memory_limit', '1024M');

        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if(count($data) == 5 && !empty($data[0]) && !empty($data[1]) && !empty($data[1]) && !empty($data[4]) &&
                    !\App\Application::where('sn', $data[3])->where('app_id',$data[0])->first()){
                    $application = new \App\Application();
                    $application->app_id = $data[0];
                    $application->app_key = $data[1];
                    $application->sn = $data[3];
                    $application->app_secret = $data[4];
                    $application->save();
                }

            }
            fclose($handle);
        }
    }
}
